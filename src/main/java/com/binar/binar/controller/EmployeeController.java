package com.binar.binar.controller;

import com.binar.binar.entity.Employee;
import com.binar.binar.model.EmployeeModel;
import com.binar.binar.repository.EmployeeRepo;
import com.binar.binar.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("v1/employee/")
public class EmployeeController {

    @Autowired //
    public EmployeeRepo repo;

    @Autowired
    EmployeeService servis;

    @Value("${app.uploadto.cdn}")//FILE_SHOW_RUL
    private String UPLOADED_FOLDER;

    @GetMapping("/listpage")
    @ResponseBody
    public ResponseEntity<Map>  getList() {
        Map c = servis.getAll();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    // step akhir DTO
    @GetMapping("/listpagenative")
    @ResponseBody
    public ResponseEntity<Map>  listpagenative() {
        Map c = servis.getAllNative();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    // step akhir DTO untuk entitas
    @GetMapping("/listpageentitas")
    @ResponseBody
    public ResponseEntity<Map>  listpageentitas() {
        Map map = new HashMap();
        List<EmployeeModel> obj = repo.modelDTO();
        map.put("data", obj);
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }


    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Employee objModel) {

        Map map = new HashMap();
        Map obj = servis.insert(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);// response
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Employee objModel) {

        Map map = new HashMap();
        Map c = servis.update(objModel);

        map.put("data", c);
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {

        Map map = new HashMap();
//        Map c = servis.delete(id);
        Employee obj = repo.getbyID(id);
        obj.setDeleted_date(new Date());
        repo.save(obj);
        map.put("data", "sukses delete");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }



    @GetMapping("/listbynama")
    public ResponseEntity<Page<Employee>> getpage2(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String name) {
        Pageable show_data = PageRequest.of(page, size);
        Page<Employee> list = repo.findByName(name, show_data);
        return new ResponseEntity<Page<Employee>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/listbynamalike")
    public ResponseEntity<Page<Employee>> listbynamalike(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String name) {
        Pageable show_data = PageRequest.of(page, size);
//        Page<Barang> list = repo.findByNamaLike(nama, show_data);
        Page<Employee> list = repo.findByNameLike("%"+name+"%", show_data);
        return new ResponseEntity<Page<Employee>>(list, new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value = "/uploadsaveemployee",consumes = {"multipart/form-data", "application/json"})
    public ResponseEntity<Map> uploadFile(@RequestParam(value="file", required = true) MultipartFile file,  @PathVariable(value = "id") Long id,
                                          @RequestParam(value="name", required = true) String name,
                                          @RequestParam(value="gender", required = true) String gender,
                                          @RequestParam(value="dob", required = true) LocalDate dob,
                                          @RequestParam(value="address", required = true) String address,
                                          @RequestParam(value="status", required = true) int status)  {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMyyyyhhmmss");
        String strDate = formatter.format(date);
        String fileName = UPLOADED_FOLDER + strDate + file.getOriginalFilename();
        String fileNameforDOwnload = strDate + file.getOriginalFilename();
        Path TO = Paths.get(fileName);
        Map map = new HashMap();
        try {
            Files.copy(file.getInputStream(), TO); // pengolahan upload disini :
            // insert barang
            Employee e = new Employee();
            e.setName(name);
            e.setGender(gender);
            e.setDob(dob);
            e.setAddress(address);
            e.setStatus(status);
            e.setFilenama(fileNameforDOwnload);
            map = servis.insert(e);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new ResponseEntity<Map>(map, HttpStatus.OK);
        }
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
}

