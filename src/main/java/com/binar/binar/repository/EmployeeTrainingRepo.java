package com.binar.binar.repository;

import com.binar.binar.entity.Employee;
import com.binar.binar.entity.EmployeeTraining;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeTrainingRepo extends JpaRepository<EmployeeTraining, Long> {
    @Query("select c from EmployeeTraining c")// nama class
    public List<EmployeeTraining> getList();
}
